
## What is Docker?

Docker is a platform which packages an application and all its dependencies together in the form of containers. This containerization aspect ensures that the application works in any environment.

![Docker](extras/Docker_Intro.png)

As you can see in the diagram, each and every application runs on separate containers and has its own set of dependencies & libraries. This makes sure that each application is independent of other applications, giving developers surety that they can build applications that will not interfere with one another.

So a developer can build a container having different applications installed on it and give it to the QA team. Then the QA team would only need to run the container to replicate the developer’s environment.

## Dockerfile, Images & Containers

Dockerfile, Docker Images & Docker Containers are three important terms that you need to understand while using Docker.

![Docker Terms](extras/Docker_terms.png)

As you can see in the above diagram when the Dockerfile is built, it becomes a Docker Image and when we run the Docker Image then it finally becomes a Docker Container.

  ### Dockerfile:

  A Dockerfile is a text document which contains all the commands that a user can call on the command line to assemble an image. So, Docker can build images automatically by reading the instructions from a Dockerfile. You can use `docker build` to create an automated build to execute several command-line instructions in succession.

  ### Docker Image: 
  In layman terms, Docker Image can be compared to a template which is used to create Docker Containers. So, these read-only templates are the building blocks of a Container. You can use `docker run` to run the image and create a container.

Docker Images are stored in the Docker Registry. It can be either a user’s local repository or a public repository like a Docker Hub which allows multiple users to collaborate in building an application.

  ### Docker Container: 
  It is a running instance of a Docker Image as they hold the entire package needed to run the application. So, these are basically the ready applications created from Docker Images which is the ultimate utility of Docker. 

## Docker Platform

Docker provides tooling and a platform to manage the lifecycle of your containers:

- Develop your application and its supporting components using containers.
- The container becomes the unit for distributing and testing your application.
- When you’re ready, deploy your application into your production environment, as a container or an orchestrated service.

## Docker Process

In simple way, Docker process is explained by given flowchart:


![Docker Process](extras/Docker_Process.png)


## Docker Commands

### Containers

Use `docker container my_command`

`create` — Create a container from an image.

`start` — Start an existing container.

`run` — Create a new container and start it.

`ls` — List running containers.

`inspect` — See lots of info about a container.

`logs` — Print logs.

`stop` — Gracefully stop running container.

`kill` —Stop main process in container abruptly.

`rm`— Delete a stopped container.

### Images

Use `docker image my_command`

`build` — Build an image.

`push` — Push an image to a remote registry.

`ls` — List images.

`history` — See intermediate image info.

`inspect` — See lots of info about an image, including the layers.

`rm` — Delete an image.

### Misc

`docker version` — List info about your Docker Client and Server versions.

`docker login` — Log in to a Docker registry.

`docker system prune` — Delete all unused containers, unused networks, and dangling images.

## Docker Example

 `docker run hello-world`

Hello from Docker.
This message shows that your installation appears to be working correctly.


